package com.creditfamily.ruleEngine.drools.facet;

import com.creditfamily.ruleEngine.drools.utils.RuleResultUtil;
import lombok.Setter;

import java.io.Serializable;

@Setter
public class NbfcCredit implements Serializable {

    /**
     * 1、是 0、否
     */
    private int hitRule = 0;

    /**
     * 命中规则
     */
    private String hitRuleTag;

    public int getHitRule() {
        RuleResultUtil.addDetailData("hitRule", hitRule);
        return hitRule;
    }

    public String getHitRuleTag() {
        RuleResultUtil.addDetailData("hitRuleTag", hitRuleTag);
        return hitRuleTag;
    }

}
