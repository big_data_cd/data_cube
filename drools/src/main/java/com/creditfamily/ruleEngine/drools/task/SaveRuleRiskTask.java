package com.creditfamily.ruleEngine.drools.task;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSONObject;
import com.creditfamily.ruleEngine.drools.engine.dto.RuleRisk;

/**
 * @author SXF
 * 2018年10月31日 14:46
 */
public class SaveRuleRiskTask implements ITask {

    private Long userId;

    private String businessId;

    private String defineCode;

    private String detailCode;

    private Object data;

    public SaveRuleRiskTask(Long userId, String businessId, String defineCode, String detailCode, Object data){
        this.userId = userId;
        this.businessId = businessId;
        this.defineCode = defineCode;
        this.detailCode = detailCode;
        this.data = data;
    }

    @Override
    public void execute() throws Exception {
        if (StringUtils.isNotBlank(businessId) && data != null) {
            RuleRisk ruleRisk = new RuleRisk();
            ruleRisk.setUserId(userId);
            ruleRisk.setBusinessId(businessId);
            ruleRisk.setRiskDefineCode(defineCode);
            ruleRisk.setRiskDetailCode(detailCode);
            ruleRisk.setData(JSONObject.toJSONString(data));
            ruleRisk.insert();
        }
    }
}
