package com.creditfamily.ruleEngine.drools.risks.nbfc;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSONObject;
import com.creditfamily.ruleEngine.drools.engine.dto.CheckParam;
import com.creditfamily.ruleEngine.drools.engine.dto.RuleRisk;
import com.creditfamily.ruleEngine.drools.facet.NbfcCredit;
import com.creditfamily.ruleEngine.drools.risks.AbstractRiskUtil;
import com.creditfamily.ruleEngine.drools.task.SaveRuleRiskTask;
import com.creditfamily.ruleEngine.drools.task.TaskManager;

public class NbfcUtil extends AbstractRiskUtil {
    private static final String defineCode = "nbfc";

    public static NbfcCredit requestCredit(CheckParam checkParam) {
        String detailCode = "credit";
        NbfcCredit nbfcCredit = null;
        RuleRisk ruleRisk = ruleRiskService.findRuleRisk(checkParam, defineCode, detailCode);
        if (ruleRisk != null) {
            nbfcCredit = JSONObject.parseObject(ruleRisk.getData(), NbfcCredit.class);
        } else {
            String nbfcData = checkParam.getNbfcData();
            Map<String, String> params = mobileThreeParams(checkParam);
            params.putAll((Map<String, String>)JSONObject.parse(nbfcData));
            Response response = new Response();
            if (Integer.valueOf(200).equals(response.getCode())) {
                nbfcCredit = genNbfcCredit(response.getData());
                TaskManager.execute(new SaveRuleRiskTask(checkParam.getUserId(), checkParam.getBusinessId(), defineCode, detailCode, nbfcCredit));
            }
        }
        return nbfcCredit;
    }

    private static NbfcCredit genNbfcCredit(String resData) {
        NbfcCredit credit = new NbfcCredit();
        if (StringUtils.isNotBlank(resData)) {
            String regex = ".*Rule[ABCDEF]-True.*";
            if (resData.matches(regex)) {
                credit.setHitRule(1);
                credit.setHitRuleTag(resData);
            }
        }
        return credit;
    }
    

}


class Response {
	private int code;
	
	private String Data;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getData() {
		return Data;
	}

	public void setData(String data) {
		Data = data;
	}
	
	
	
}
