package com.creditfamily.ruleEngine.drools.facet;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.creditfamily.ruleEngine.drools.engine.dto.CheckParam;
import com.creditfamily.ruleEngine.drools.risks.nbfc.NbfcUtil;
import com.creditfamily.ruleEngine.drools.utils.RuleResultUtil;

import lombok.Setter;

/**
 * @author SXF
 * 2018年09月21日 16:57
 */
@Setter
public class RiskInfo implements Serializable {

    /**
     * 当前执行的规则代码
     */
    private String currentRuleCode;

    /**
     * 参数,用于查询征信, 请勿在规则中使用
     */
    private CheckParam checkParam;

    /**
     * 扩展信息
     */
    private Map<String, Object> extendInfo = new HashMap<>();

    private NbfcCredit nbfcCredit;

   


    public NbfcCredit getNbfcCredit() {
        if (nbfcCredit == null) {
            nbfcCredit = NbfcUtil.requestCredit(checkParam);
        }
        return nbfcCredit;
    }
 

    public static Object getExtendInfo(Map<String, Object> info, String key) {
        Object desc = info.get(key + "Desc");
        if (desc == null) {
            desc = key;
        }
        Object value = info.get(key);
        RuleResultUtil.addDetailData(String.valueOf(desc), value);
        return value;
    }

    public static String getExtendString(Map<String, Object> info, String key) {
        return (String) getExtendInfo(info, key);
    }

    public static Integer getExtendInteger(Map<String, Object> info, String key) {
        Object value = getExtendInfo(info, key);
        return value == null ?  null : Integer.valueOf(String.valueOf(value));
    }

    public static Long getExtendLong(Map<String, Object> info, String key) {
        Object value = getExtendInfo(info, key);
        return value == null ?  null : Long.valueOf(String.valueOf(value));
    }

    public static Double getExtendDouble(Map<String, Object> info, String key) {
        Object value = getExtendInfo(info, key);
        return value == null ?  null : Double.valueOf(String.valueOf(value));
    }

    public String getExtendString(String key) {
        return (String) getExtendInfo(extendInfo, key);
    }

    public Integer getExtendInteger(String key) {
        Object value = getExtendInfo(extendInfo, key);
        return value == null ?  null : Integer.valueOf(String.valueOf(value));
    }

    public Long getExtendLong(String key) {
        Object value = getExtendInfo(extendInfo, key);
        return value == null ?  null : Long.valueOf(String.valueOf(value));
    }

    public Double getExtendDouble(String key) {
        Object value = getExtendInfo(extendInfo, key);
        return value == null ?  null : Double.valueOf(String.valueOf(value));
    }
}
