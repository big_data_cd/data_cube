package com.creditfamily.ruleEngine.drools.engine.dto;

import java.io.Serializable;
import java.util.LinkedList;

/**
 * @author SXF
 * 2018年09月21日 16:56
 */
public class RuleResult implements Serializable {

    private int code = 200;           // 代码

    private String currentCode; // 当前规则代码

    private boolean hitRule;    // 是否命中规则

    private LinkedList<ResultDetail> detailList;       // 规则命中详情

    public String getCurrentCode() {
        return currentCode;
    }

    public void setCurrentCode(String currentCode) {
        this.currentCode = currentCode;
    }

    public boolean isHitRule() {
        return hitRule;
    }

    public void setHitRule(boolean hitRule) {
        this.hitRule = hitRule;
    }

    public LinkedList<ResultDetail> getDetailList() {
        return detailList;
    }

    public void setDetailList(LinkedList<ResultDetail> detailList) {
        this.detailList = detailList;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
