package com.creditfamily.ruleEngine.drools.utils;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.creditfamily.ruleEngine.drools.engine.EngineHelper;
import com.creditfamily.ruleEngine.drools.engine.dto.DetailData;
import com.creditfamily.ruleEngine.drools.engine.dto.ResultDetail;
import com.creditfamily.ruleEngine.drools.engine.dto.RuleResult;

/**
 * @author SXF
 * 2018年09月28日 17:30
 */
public class RuleResultUtil {

    public static <T> void addDetailData(String key, T value) {
        if (StringUtils.isBlank(key)) {
            return;
        }
        String cacheKey = Thread.currentThread().getName();
        RuleResult runInfo = EngineHelper.getRunInfo(cacheKey);
        if (runInfo != null) {
            ResultDetail last = runInfo.getDetailList().getLast();
            List<DetailData> dataList = last.getDataList();
            boolean repeat = false;
            if (CollectionUtils.isNotEmpty(dataList)){
                for (DetailData detailData : dataList) {
                    if (detailData.getKey().equals(key)) {
                        repeat = true;
                        break;
                    }
                }
            }
            if (!repeat) {
                dataList.add(new DetailData(key, value));
            }
        }
    }
}
