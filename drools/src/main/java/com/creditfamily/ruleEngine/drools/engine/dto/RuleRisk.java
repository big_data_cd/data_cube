package com.creditfamily.ruleEngine.drools.engine.dto;

import java.io.Serializable;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.FieldFill;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author Administrator123
 * @since 2018-10-26
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("cf_re_rule_risk")
public class RuleRisk extends Model<RuleRisk> {

    private static final long serialVersionUID = 1L;

    private Long id;
    /**
     * 用户Id
     */
    private Long userId;
    /**
     * 业务ID
     */
    private String businessId;
    /**
     * riskDefineCode
     */
    private String riskDefineCode;
    /**
     * riskDetailCode
     */
    private String riskDetailCode;
    /**
     * 原始数据
     */
    private String data;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
