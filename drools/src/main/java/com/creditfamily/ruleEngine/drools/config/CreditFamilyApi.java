package com.creditfamily.ruleEngine.drools.config;

/**
 * @author qiaoyao
 * @version 1.0
 * @date 2020年4月22日 
 * Copyright 深圳市信用之家金融服务有限公司  xyzj All Rights Reserved
 * 官方网站: http://www.creditfamily.cn/
 */
public interface CreditFamilyApi {

	String getUrl();
}

