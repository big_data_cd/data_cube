package com.creditfamily.ruleEngine.drools.task;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.*;

/**
 * @author SXF
 * 2017年12月18日 15:57
 */
@Slf4j
public class TaskManager {

    private static ExecutorService threadPool = Executors.newCachedThreadPool();

    private static ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(100);

    public static void execute(final ITask task) {
        threadPool.execute(() -> {
            try {
                task.execute();
            } catch (Exception e) {
                log.error("任务执行失败", e);
            }
        });
    }

    public static void execute(final ITask task, long delay, TimeUnit timeUnit) {
        scheduledExecutorService.schedule(() -> {
            try {
                task.execute();
            } catch (Exception e) {
                log.error("任务执行失败", e);
            }
        }, delay, timeUnit);
    }

    public static <T> Future<T> submit(final ICallback<T> callback) throws Exception {
        return threadPool.submit(callback::callback);
    }
}

