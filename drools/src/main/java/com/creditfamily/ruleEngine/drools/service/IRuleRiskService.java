package com.creditfamily.ruleEngine.drools.service;

import com.baomidou.mybatisplus.service.IService;
import com.creditfamily.ruleEngine.drools.engine.dto.CheckParam;
import com.creditfamily.ruleEngine.drools.engine.dto.RuleRisk;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Administrator123
 * @since 2018-10-26
 */
public interface IRuleRiskService extends IService<RuleRisk> {

    RuleRisk findRuleRisk(CheckParam checkParam, String detailCode, String businessId);

}
