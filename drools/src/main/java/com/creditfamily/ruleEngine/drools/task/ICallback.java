package com.creditfamily.ruleEngine.drools.task;

/**
 * @author SXF
 * 2018年09月25日 11:29
 */
public interface ICallback<T> {

   T callback() throws Exception;
}
