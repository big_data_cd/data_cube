package com.creditfamily.ruleEngine.drools.config;

/**
 * @author SXF
 * 2018年09月26日 17:10
 */
public class ProductCode {


    public enum Base implements CreditFamilyApi {
        Login("/api/login"),
        ;
        private String url;
        Base(String url){
            this.url = url;
        }
        @Override
        public String getUrl() {
            return this.url;
        }
    }

 

    public enum Nbfc implements CreditFamilyApi {
        Credit("/api/risk/nbfc/credit"),
        ;
        private String url;
        Nbfc(String url) {
            this.url = url;
        }

        @Override
        public String getUrl() {
            return this.url;
        }
    }

    

}
