package com.creditfamily.ruleEngine.drools.task;


/**
 * @author SXF
 * 2017年12月18日 15:59
 */
public interface ITask {

    void execute() throws Exception;
}
