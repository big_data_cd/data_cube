package com.creditfamily.ruleEngine.drools.engine.dto;

import java.io.Serializable;

/**
 * @author SXF
 * 2018年09月26日 14:40
 */
public class DetailData<T> implements Serializable {
    private String key;
    private T value;

    public DetailData(){}

    public DetailData(String key, T value) {
        this.key = key;
        this.value = value;
        if (value == null) {
            this.value = (T) "无数据";
        }
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
}
