package com.creditfamily.ruleEngine.drools.risks;

import java.util.HashMap;
import java.util.Map;

import com.creditfamily.ruleEngine.drools.engine.dto.CheckParam;
import com.creditfamily.ruleEngine.drools.service.IRuleRiskService;
import com.creditfamily.ruleEngine.drools.utils.SpringContextUtil;

/**
 * @author SXF
 * 2018年10月22日 16:32
 */
public class AbstractRiskUtil {

	protected static IRuleRiskService ruleRiskService = SpringContextUtil.getBean(IRuleRiskService.class);;

    protected static Map<String, String> idTwoParams(CheckParam checkParam) {
        Map<String, String> params = new HashMap<>();
        params.put("name", checkParam.getName());
        params.put("idCard", checkParam.getIdCard());
        params.put("businessId", checkParam.getBusinessId());
        params.put("accessToken", checkParam.getRAccessToken());
        params.put("aesKey", checkParam.getRAesKey());
        return params;
    }

    protected static Map<String, String> mobileThreeParams(CheckParam checkParam) {
        Map<String, String> params = idTwoParams(checkParam);
        params.put("phone", checkParam.getPhone());
        return params;
    }

    protected static Map<String, String> bankCardFourParams(CheckParam checkParam) {
        Map<String, String> params = mobileThreeParams(checkParam);
        params.put("bankAccount", checkParam.getBankAccount());
        return params;
    }
}
