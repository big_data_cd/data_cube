package com.creditfamily.ruleEngine.drools.engine.dto;

import java.io.Serializable;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.FieldFill;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author Administrator123
 * @since 2018-08-30
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("cf_re_rule_group")
public class RuleGroup extends Model<RuleGroup> {

    private static final long serialVersionUID = 1L;

    private Long id;
    /**
     * 用户ID
     */
    private Long userId;
    /**
     * 规则组名称
     */
    private String groupName;
    /**
     * 规则组代码
     */
    private String groupCode;
    /**
     * 规则组描述
     */
    private String groupDesc;
    /**
     * 何时停止 命中 pass/refuse
     */
    private String stopAt;
    /**
     * 规则组状态
     */
    private String valid;
    /**
     * 创建人
     */
    @TableField(fill = FieldFill.INSERT)
    private Long createUser;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    /**
     * 修改人
     */
    @TableField(fill = FieldFill.UPDATE)
    private Long updateUser;
    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.UPDATE)
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
