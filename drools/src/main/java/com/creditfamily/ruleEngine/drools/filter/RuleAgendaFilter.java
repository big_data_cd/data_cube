package com.creditfamily.ruleEngine.drools.filter;

import org.kie.api.definition.rule.Rule;
import org.kie.api.runtime.rule.AgendaFilter;
import org.kie.api.runtime.rule.Match;

/**
 * @author SXF
 * 2018年09月25日 14:40
 */
public class RuleAgendaFilter implements AgendaFilter {

    private String rulePackage;

    private String ruleName;

    public RuleAgendaFilter(String rulePackage, String ruleName) {
        this.rulePackage = rulePackage;
        this.ruleName = ruleName;
    }

    @Override
    public boolean accept(Match match) {
        Rule rule = match.getRule();
        return rule.getPackageName().equals(this.rulePackage) && rule.getName().equals(this.ruleName);
    }
}
