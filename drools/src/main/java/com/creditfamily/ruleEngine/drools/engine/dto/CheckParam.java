package com.creditfamily.ruleEngine.drools.engine.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author SXF
 * 2018年09月21日 17:38
 */
@Getter
@Setter
@Accessors(chain = true)
public class CheckParam implements Serializable {
    /**
     * 用户Id
     */
    private Long userId;
    /**
     * 业务ID, 必填
     */
    private String businessId;
    /**
     * 规则组代码
     */
    private String ruleGroupCode;
    /**
     * 姓名
     */
    private String name;
    /**
     * 身份证号
     */
    private String idCard;
    /**
     * 手机号
     */
    private String phone;
    /**
     * 银行卡号
     */
    private String bankAccount;
    /**
     * 访问征信中心AccessToken
     */
    private String rAccessToken;
    /**
     * 访问征信中心AesKey
     */
    private String rAesKey;
    /**
     * 扩展参数,Json串
     */
    private String extendData;
    /**
     * nbcfData, json串
     */
    private String nbfcData;
    /**
     * 身份证类别
     */
    private String idType;
    /**
     * 设备指纹
     */
    private String blackBox;

    private String panNum;

}
