/**
 * Copyright (C), 2015-2019, 信用之家金融服务有限公司
 * FileName: PpxRisklist
 * Author:   Administrator
 * Date:     2019/5/27 11:41
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.creditfamily.ruleEngine.drools.facet;

import com.creditfamily.ruleEngine.drools.utils.RuleResultUtil;
import lombok.Setter;

import java.io.Serializable;

/**
 * 风险名单详版
 */
@Setter
public class PpxRisklist implements Serializable {
    /**
     * 是否是不良用户 输出：1（是），2（否）
     */
    private String isBlack;
    /**
     * 是否是关注用户 输出：1（是），2（否）
     */
    private String isAlert;
    /**
     * 命中规则
     */
    private String hitRules;

    public String getIsBlack() {
        RuleResultUtil.addDetailData("是否不良用户", isBlack);
        return isBlack;
    }

    public String getIsAlert() {
        RuleResultUtil.addDetailData("是否关注用户", isAlert);
        return isAlert;
    }

    public String getHitRules() {
        RuleResultUtil.addDetailData("命中规则", hitRules);
        return hitRules;
    }
}
