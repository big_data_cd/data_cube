package com.creditfamily.ruleEngine.drools.engine;

import java.util.concurrent.ConcurrentHashMap;

import org.kie.api.runtime.KieContainer;

import com.creditfamily.ruleEngine.drools.engine.dto.CheckParam;
import com.creditfamily.ruleEngine.drools.engine.dto.RuleResult;
import com.creditfamily.ruleEngine.drools.utils.SpringContextUtil;

/**
 * @author SXF
 * 2018年09月25日 17:59
 */
public class EngineHelper {

    private static final KieContainer kieContainer = SpringContextUtil.getBean(KieContainer.class);

    private static final ConcurrentHashMap<String, RuleResult> runInfoMap = new ConcurrentHashMap<>();

    private static final ConcurrentHashMap<String, CheckParam> paramInfoMap = new ConcurrentHashMap<>();

    public static KieContainer getKieContainer() {
        return kieContainer;
    }

    public static RuleResult getRunInfo(String key) {
        return runInfoMap.get(key);
    }

    public static RuleResult setRunInfo(String key, RuleResult ruleResult) {
        return runInfoMap.put(key, ruleResult);
    }

    public static void removeRunInfo(String key) {
        runInfoMap.remove(key);
    }

    public static CheckParam getParamInfo(String key) {
        return paramInfoMap.get(key);
    }

    public static CheckParam setParamInfo(String key, CheckParam checkParam) {
        return paramInfoMap.put(key, checkParam);
    }

    public static void removeParamInfo(String key) {
        paramInfoMap.remove(key);
    }
}
