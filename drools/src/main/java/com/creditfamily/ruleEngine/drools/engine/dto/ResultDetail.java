package com.creditfamily.ruleEngine.drools.engine.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author SXF
 * 2018年09月26日 14:39
 */
public class ResultDetail implements Serializable {
    private String ruleCode;

    private String ruleName;

    private String ruleDesc;

    private boolean hitRule;

    private List<DetailData> dataList = new ArrayList<>();

    public String getRuleCode() {
        return ruleCode;
    }

    public void setRuleCode(String ruleCode) {
        this.ruleCode = ruleCode;
    }

    public List<DetailData> getDataList() {
        return dataList;
    }

    public void setDataList(List<DetailData> dataList) {
        this.dataList = dataList;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public String getRuleDesc() {
        return ruleDesc;
    }

    public void setRuleDesc(String ruleDesc) {
        this.ruleDesc = ruleDesc;
    }

    public boolean isHitRule() {
        return hitRule;
    }

    public void setHitRule(boolean hitRule) {
        this.hitRule = hitRule;
    }
}
